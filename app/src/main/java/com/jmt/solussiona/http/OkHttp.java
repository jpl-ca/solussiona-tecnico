package com.jmt.solussiona.http;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.jmt.solussiona.model.entity.SessionE;
import com.jmt.solussiona.model.store.Jmstore;
import com.jmt.solussiona.util.S;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.Headers;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import java.util.Iterator;
public class OkHttp {
    public static String URL="http://192.168.1.100";
//    public static String URL="http://45.55.237.221";
    Context ctx;
    private final OkHttpClient client = new OkHttpClient();
    public Response makeGetSynchronous(String url) throws IOException {
        Request request = prepare_request(url).build();
        return client.newCall(request).execute();
    }
    public void makeGetASynchronous(String url) throws IOException {
        Request request = prepare_request(url).build();
        client.newCall(request).enqueue(new Callback(){
            @Override
            public void onFailure(Request request, IOException e) {}
            @Override public void onResponse(Response response) throws IOException {}
        });
    }
    Response makePostRequest(String url, String json) throws IOException {
        FormEncodingBuilder fe = new FormEncodingBuilder();
        try {
            JSONObject jo = new JSONObject(json);
            Iterator<?> keys = jo.keys();
            while( keys.hasNext() ) {
                String key = (String)keys.next();
                fe.add(key,jo.getString(key));
            }
        }catch (JSONException e){}
        RequestBody body2 = fe.build();
        Request request = prepare_request(url).post(body2).build();
        return client.newCall(request).execute();
    }
    public Request.Builder prepare_request(String url){
        Jmstore js = new Jmstore(ctx);
        String gcm_id = js.get(S.GCMID);
        return new Request.Builder()
            .url(url)
            .addHeader("Cookie", js.get(S.COOKIE))
            .addHeader(S.TOKEN, js.get(S.TOKEN))
            .addHeader("device", "mobile")
            .addHeader(S.GCMID, gcm_id);
    }
    public void showResponse(Response response) {
        if (!response.isSuccessful()) System.out.println("Unexpected code " + response);
        Headers responseHeaders = response.headers();
        for (int i = 0; i < responseHeaders.size(); i++){
            if(responseHeaders.name(i).equals(S.COOKIE)){
                new Jmstore(ctx).push(S.COOKIE, responseHeaders.value(i));
            }
        }
    }
    public OkHttp(Context ctx){
        this.ctx = ctx;
    }
    public String makeGetRequest(String service,boolean ServerExtern) {
        String resp = "{}";
        if(!ServerExtern)
        service=URL+service;
        System.out.println(service);
        try {
            Response r = makeGetSynchronous(service);
            showResponse(r);
            if(r.isSuccessful())
                resp = r.body().string();
        }catch (IOException e){
            SessionE ss = new SessionE();
            ss.setStatus(false);
            ss.setMessage("-1");
            resp = new Gson().toJson(ss);
        }
        return resp;
    }

    public String makePostRequest(String service,JSONObject params) {
        String resp = "{}";
        service=URL+service;
        System.out.println(service);
        try {
            Response r = makePostRequest(service,params.toString());
            showResponse(r);
            if(r.isSuccessful()) resp = r.body().string();
        }catch (IOException e){
            SessionE ss = new SessionE();
            ss.setStatus(false);
            ss.setMessage("-1");
            resp = new Gson().toJson(ss);
        }
        return resp;
    }
}