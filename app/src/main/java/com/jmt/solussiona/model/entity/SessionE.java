package com.jmt.solussiona.model.entity;

/**
 * Created by JMTech-Android on 18/03/2015.
 */
public class SessionE{
    boolean status;
    boolean success_request;
    String message;

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public boolean getSuccess_request() {
        return success_request;
    }

    public void setSuccess_request(boolean success_request) {
        this.success_request = success_request;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}