package com.jmt.solussiona.model.entity;

/**
 * Created by JMTech-Android on 18/03/2015.
 */
public class ServiceOrderE{
    int so_id;
    int so_state;
    int sr_id;
    int user_id;
    String user_firstname;
    String user_lastname;
    String sr_address;
    String user_phone;
    int state_new;
    so_created so_created;

    public int getSo_id() {
        return so_id;
    }

    public void setSo_id(int so_id) {
        this.so_id = so_id;
    }

    public int getSo_state() {
        return so_state;
    }

    public void setSo_state(int so_state) {
        this.so_state = so_state;
    }

    public int getSr_id() {
        return sr_id;
    }

    public void setSr_id(int sr_id) {
        this.sr_id = sr_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getUser_firstname() {
        return user_firstname;
    }

    public void setUser_firstname(String user_firstname) {
        this.user_firstname = user_firstname;
    }

    public String getUser_lastname() {
        return user_lastname;
    }

    public void setUser_lastname(String user_lastname) {
        this.user_lastname = user_lastname;
    }

    public String getUser_address() {
        return sr_address;
    }

    public void setUser_address(String user_address) {
        this.sr_address = user_address;
    }

    public String getUser_phone() {
        return user_phone;
    }

    public void setUser_phone(String user_phone) {
        this.user_phone = user_phone;
    }

    public int getState_new() {
        return state_new;
    }

    public void setState_new(int state_new) {
        this.state_new = state_new;
    }

    public String getSr_address() {
        return sr_address;
    }

    public void setSr_address(String sr_address) {
        this.sr_address = sr_address;
    }

    public ServiceOrderE.so_created getSo_created() {
        return so_created;
    }

    public void setSo_created(ServiceOrderE.so_created so_created) {
        this.so_created = so_created;
    }

    public class so_created{
        String date;
        String timezone_type;
        String timezone;

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getTimezone_type() {
            return timezone_type;
        }

        public void setTimezone_type(String timezone_type) {
            this.timezone_type = timezone_type;
        }

        public String getTimezone() {
            return timezone;
        }

        public void setTimezone(String timezone) {
            this.timezone = timezone;
        }
    }
}