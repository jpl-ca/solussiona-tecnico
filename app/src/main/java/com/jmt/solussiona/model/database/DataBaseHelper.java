package com.jmt.solussiona.model.database;
/**
 * Created by JMTech-Android on 12/02/2015.
 */
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DataBaseHelper extends SQLiteOpenHelper{
    private static final String LOG = "DatabasHelper";
    private static final String DATABASE_NAME = "dbsolusiona_1.0.3";
    private static final int DATABASE_VERSION = 1;

    private static DataBaseHelper instance=null;

    public static synchronized DataBaseHelper getHelper(Context context) {
        if (instance == null)
            instance = new DataBaseHelper(context);
        return instance;
    }

    private DataBaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
        if (!db.isReadOnly()) {
            db.execSQL("PRAGMA foreign_keys=ON;");
        }
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DB.CREATE_TABLE_TECNICO);
        db.execSQL(DB.CREATE_TABLE_ORDEN_SERVICIO);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS" + DB.TB_TECNICO);
        db.execSQL("DROP TABLE IF EXISTS" + DB.TB_ORDEN_SERVICIO);
        onCreate(db);
    }
}