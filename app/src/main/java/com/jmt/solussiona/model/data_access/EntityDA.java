package com.jmt.solussiona.model.data_access;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import com.jmt.solussiona.model.database.DataBaseHelper;
import com.jmt.solussiona.model.store.Jmstore;
import java.util.Date;

/**
 * Created by JMTech-Android on 18/03/2015.
 */
public class EntityDA {
    Context ctx;
    String LOG;
    protected DataBaseHelper nHelper;
    protected SQLiteDatabase nBD;
    protected String TB_PRINCIPAL, ID_TABLA;
    Jmstore Store;
    public EntityDA(Context context, String TB, String ID){
        ctx=context;
        LOG=context.getClass().getName();
        TB_PRINCIPAL= TB;
        ID_TABLA= ID;
        Store = new Jmstore(ctx);
    }
    public EntityDA abrir(){
        try {
            nHelper=DataBaseHelper.getHelper(ctx);
            nBD=nHelper.getWritableDatabase();
        } catch (Exception e) {
        }
        return this;
    }
    public void cerrar(){
        // TODO Comentado para Pruebas
//        nHelper.getHelper(ctx).close();
    }

    public long Delete(int Id) {
        abrir();
        ContentValues cv=new ContentValues();
        long r=nBD.delete(TB_PRINCIPAL, ID_TABLA+"="+Id, null);
        cerrar();
        return r;
    }
    public void DeleteAll() {
        abrir();
        nBD.execSQL("delete from "+ TB_PRINCIPAL+";");
        cerrar();
    }
    public void setLastUpdate(){
        Store.push(TB_PRINCIPAL + "_TIME_UPD", new Date().getTime() + "");
    }
    public int getLastUpdate(){
        String r = Store.get(TB_PRINCIPAL + "_TIME_UPD");
        long tm = 0;
        if(!r.equals("")&&!r.equals("0"))tm=new Date().getTime()-Long.parseLong(r);
        int lastUpd=(int)(tm/60000);
        Log.d(LOG, "Updated " + lastUpd + " min ago.");
        return lastUpd;
    }
}