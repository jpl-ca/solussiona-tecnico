package com.jmt.solussiona.model.entity;

/**
 * Created by JMTech-Android on 18/03/2015.
 */
public class NotificacionE{
    int notification_id;
    String title;
    String msj;
    int state;
    int so_id;
    int sr_id;

    public NotificacionE(String title, String msj, int state, int so_id, int sr_id){
        this.title=title;
        this.msj=msj;
        this.state=state;
        this.so_id=so_id;
        this.sr_id=sr_id;
    }

    public int getNotification_id() {
        return notification_id;
    }

    public void setNotification_id(int notification_id) {
        this.notification_id = notification_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMsj() {
        return msj;
    }

    public void setMsj(String msj) {
        this.msj = msj;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public int getSo_id() {
        return so_id;
    }

    public void setSo_id(int so_id) {
        this.so_id = so_id;
    }

    public int getSr_id() {
        return sr_id;
    }

    public void setSr_id(int sr_id) {
        this.sr_id = sr_id;
    }
}