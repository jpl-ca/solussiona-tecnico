package com.jmt.solussiona.model.data_access;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.google.gson.Gson;
import com.jmt.solussiona.model.database.DB;
import com.jmt.solussiona.model.entity.TecnicoE;
import com.jmt.solussiona.util.S;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by JMTech-Android on 18/03/2015.
 */
public class TecnicoDA extends EntityDA{
    public TecnicoDA(Context context){
        super(context, DB.TB_TECNICO, S.TECNICO.id);
    }

    public void Insert(ArrayList<TecnicoE> Obj) {
        abrir();
        for(TecnicoE usu : Obj)
            nBD.insert(TB_PRINCIPAL, null, prepare(usu));
        cerrar();
        setLastUpdate();
    }
    public long Insert(TecnicoE Obj) {
        DeleteAll();
        abrir();
        long r=nBD.insert(TB_PRINCIPAL, null, prepare(Obj));
        cerrar();
        setLastUpdate();
        return r;
    }
    public ContentValues prepare(TecnicoE Obj){
        ContentValues cv=new ContentValues();
        cv.put(S.TECNICO.id, Obj.getId());
        cv.put(S.TECNICO.firstname, Obj.getFirstname());
        cv.put(S.TECNICO.lastname, Obj.getLastname());
        cv.put(S.TECNICO.email, Obj.getEmail());
        cv.put(S.TECNICO.phone, Obj.getPhone());
        cv.put(S.TECNICO.mobile, Obj.getMobile());
        cv.put(S.TECNICO.imageProfile, Obj.getImageProfile());
        cv.put(S.TECNICO.latitude, Obj.getLatitude());
        cv.put(S.TECNICO.longitude, Obj.getLongitude());
        cv.put(S.TECNICO.state, Obj.getState());
        cv.put(S.TECNICO.entity_type, Obj.getEntity_type());
        cv.put(S.TECNICO.access_type, Obj.getAccess_type());
        cv.put(S.TECNICO.password, Obj.getPassword());
        cv.put(S.TECNICO.profession_professional_id, Obj.getProfession_professional_id());
        cv.put(S.TECNICO.profession_professional_name, Obj.getProfession_professional_name());
        return cv;
    }

    public TecnicoE SelectById(int Id) {
        abrir();
        String selectQuery = "SELECT  * FROM " + TB_PRINCIPAL +" WHERE "+ID_TABLA+" = "+Id;
        Cursor cursor = nBD.rawQuery(selectQuery, null);
        TecnicoE objE = new TecnicoE();
        if (cursor.moveToFirst()) {
            while (cursor.isAfterLast() == false) {
                objE=getTecnicoE(cursor);
                cursor.moveToNext();
            }
        }
        cerrar();
        return objE;
    }
    public ArrayList<TecnicoE> Select() {
        abrir();
        ArrayList<TecnicoE> list=new ArrayList<TecnicoE>();
        String selectQuery = "SELECT  * FROM " + TB_PRINCIPAL +" ORDER BY "+ID_TABLA;
        Cursor cursor = nBD.rawQuery(selectQuery, null);
        TecnicoE objE;
        if (cursor.moveToFirst()) {
            while (cursor.isAfterLast() == false) {
                objE=getTecnicoE(cursor);
                list.add(objE);
                cursor.moveToNext();
            }
        }
        cerrar();
        return list;
    }
    public TecnicoE getTecnicoE(Cursor cursor){
        int totalColumn = cursor.getColumnCount();
        JSONObject rowObject = new JSONObject();
        for (int i = 0; i < totalColumn; i++) {
            if (cursor.getColumnName(i) != null) {
                try {
                    rowObject.put(cursor.getColumnName(i),cursor.getString(i));
                } catch (Exception e) {
                }
            }
        }
        TecnicoE tec = new Gson().fromJson(rowObject.toString(),TecnicoE.class);
        return tec;
    }
    public void setIdGCM(String gcm){
        Store.push(S.GOOGLE.GCM_ID,gcm);
    }
    public String getIdGCM(){
        return Store.get(S.GOOGLE.GCM_ID);
    }
}