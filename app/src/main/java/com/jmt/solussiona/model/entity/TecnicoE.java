package com.jmt.solussiona.model.entity;

/**
 * Created by JMTech-Android on 31/03/2015.
 */
public class TecnicoE {
    int id;
    String firstname;
    String lastname;
    String email;
    String phone;
    String mobile;
    String imageProfile;
    double latitude;
    double longitude;
    int state;
    String entity_type;
    String access_type;
    String password;
    int profession_professional_id;
    String profession_professional_name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getImageProfile() {
        return imageProfile;
    }

    public void setImageProfile(String imageProfile) {
        this.imageProfile = imageProfile;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public String getEntity_type() {
        return entity_type;
    }

    public void setEntity_type(String entity_type) {
        this.entity_type = entity_type;
    }

    public String getAccess_type() {
        return access_type;
    }

    public void setAccess_type(String access_type) {
        this.access_type = access_type;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getProfession_professional_id() {
        return profession_professional_id;
    }

    public void setProfession_professional_id(int profession_professional_id) {
        this.profession_professional_id = profession_professional_id;
    }

    public String getProfession_professional_name() {
        return profession_professional_name;
    }

    public void setProfession_professional_name(String profession_professional_name) {
        this.profession_professional_name = profession_professional_name;
    }
}