package com.jmt.solussiona.model.data_access;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.jmt.solussiona.model.database.DB;
import com.jmt.solussiona.model.entity.ServiceOrderE;
import com.jmt.solussiona.util.S;
import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 * Created by JMTech-Android on 18/03/2015.
 */
public class OrdenServicioDA extends EntityDA{
    public OrdenServicioDA(Context context){
        super(context, DB.TB_ORDEN_SERVICIO, S.ORDEN.so_id);
    }

    public void Insert(int estado, ArrayList<ServiceOrderE> Obj) {
        abrir();
        ContentValues cv=new ContentValues();
        cv.put(S.ORDEN.so_state, estado);
        cv.put(S.ORDEN.detail, new Gson().toJson(Obj));
        nBD.insert(TB_PRINCIPAL, null, cv);
        cerrar();
        setLastUpdate();
    }

    public ArrayList<ServiceOrderE> getPending(){
        String selectQuery = "SELECT  * FROM " + TB_PRINCIPAL +" WHERE "+S.ORDEN.so_state+"=0";
        return getSolicitud(selectQuery);
    }
    public ArrayList<ServiceOrderE> getAcepted(){
        String selectQuery = "SELECT  * FROM " + TB_PRINCIPAL +" WHERE "+S.ORDEN.so_state+"=1";
        return getSolicitud(selectQuery);
    }
    public ArrayList<ServiceOrderE> getConfirmed(){
        String selectQuery = "SELECT  * FROM " + TB_PRINCIPAL +" WHERE "+S.ORDEN.so_state+"=2";
        return getSolicitud(selectQuery);
    }
    public ArrayList<ServiceOrderE> getCompleted(){
        String selectQuery = "SELECT  * FROM " + TB_PRINCIPAL +" WHERE "+S.ORDEN.so_state+"=3";
        return getSolicitud(selectQuery);
    }
    public ArrayList<ServiceOrderE> getCanceled(){
        String selectQuery = "SELECT  * FROM " + TB_PRINCIPAL +" WHERE "+S.ORDEN.so_state+"=4";
        return getSolicitud(selectQuery);
    }

    private ArrayList<ServiceOrderE> getSolicitud(String selectQuery) {
        abrir();
        ArrayList<ServiceOrderE> list=new ArrayList<ServiceOrderE>();
        Cursor cursor = nBD.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            while (cursor.isAfterLast() == false) {
                String detalle = cursor.getString(cursor.getColumnIndex(S.ORDEN.detail));
                Type type = new TypeToken<ArrayList<ServiceOrderE>>(){}.getType();
                list = new Gson().fromJson(detalle,type);
                cursor.moveToNext();
            }
        }
        cerrar();
        return list;
    }
}