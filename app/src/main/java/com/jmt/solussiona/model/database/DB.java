package com.jmt.solussiona.model.database;
import com.jmt.solussiona.util.S;
public class DB {
    // TABLE NAMES
    public static final String TB_TECNICO = "tb_tecnico";
    public static final String TB_ORDEN_SERVICIO = "tb_solicitud";
    public static final String CREATE_TABLE_TECNICO = "CREATE TABLE " + TB_TECNICO
            + "(" + S.TECNICO.id+ " INTEGER PRIMARY KEY, "
            + S.TECNICO.firstname + " TEXT, " + S.TECNICO.lastname + " TEXT, " + S.TECNICO.email + " TEXT, "
            + S.TECNICO.phone+ " TEXT, " + S.TECNICO.mobile + " TEXT, " + S.TECNICO.imageProfile+ " TEXT, "
            + S.TECNICO.latitude + " TEXT, " + S.TECNICO.longitude  + " TEXT, " + S.TECNICO.state+ " INTEGER,"
            + S.TECNICO.entity_type + " TEXT, " + S.TECNICO.password + " TEXT, " + S.TECNICO.access_type+ " TEXT,"
            + S.TECNICO.profession_professional_id + " INTEGER, " + S.TECNICO.profession_professional_name+ " TEXT)";
    public static final String CREATE_TABLE_ORDEN_SERVICIO= "CREATE TABLE " + TB_ORDEN_SERVICIO
            + "(" + S.ORDEN.so_state + " INTEGER,"
            + S.ORDEN.detail + " TEXT)";
}