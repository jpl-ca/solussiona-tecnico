package com.jmt.solussiona.model.store;

import android.content.Context;
import android.content.SharedPreferences;

import com.jmt.solussiona.util.S;

/**
 * Created by JMTech-Android on 13/03/2015.
 */
public class Jmstore {
    SharedPreferences DB;
    public Jmstore(Context ctx){
        DB= ctx.getSharedPreferences("sp_solussiona",Context.MODE_PRIVATE);
    }
    public void push(String key,String value){
        SharedPreferences.Editor editor = DB.edit();
        editor.putString(key, value);
        editor.commit();
    }
    public String get(String key) {
        if(DB.contains(key)) {
            return DB.getString(key, "");
        }
        return "";
    }

    public void remove(String key){
        SharedPreferences.Editor editor = DB.edit();
        editor.remove(key);
        editor.commit();
    }

    public void remove(){
        String gcm_id = get(S.GCMID);
        SharedPreferences.Editor editor = DB.edit();
        editor.clear();
        editor.commit();
        push(S.GCMID,gcm_id);
    }

    public int nextInt() {
        int c=1;
        String ky = "next_val";
        if(DB.contains(ky))
            c += DB.getInt(ky,0);
        SharedPreferences.Editor editor = DB.edit();
        editor.putInt(ky, c);
        editor.commit();
        return c;
    }
}