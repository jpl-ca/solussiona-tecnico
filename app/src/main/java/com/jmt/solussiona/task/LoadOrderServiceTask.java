package com.jmt.solussiona.task;

import android.content.Context;
import android.os.AsyncTask;
import com.google.gson.Gson;
import com.jmt.solussiona.http.OkHttp;
import com.jmt.solussiona.model.data_access.OrdenServicioDA;
import com.jmt.solussiona.model.data_access.TecnicoDA;
import com.jmt.solussiona.model.entity.ServiceOrderE;
import com.jmt.solussiona.model.entity.TecnicoE;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;

public class LoadOrderServiceTask extends AsyncTask<String, String, String>{
    Context ctx;
    public LoadOrderServiceTask(Context ctx){
        this.ctx = ctx;
    }
    @Override
    protected void onPreExecute(){
        super.onPreExecute();
    }
    @Override
    protected String doInBackground(String... params) {
        JSONObject Z = new JSONObject();
        try{
            TecnicoE t = new TecnicoDA(ctx).Select().get(0);
            OrdenServicioDA sDA = new OrdenServicioDA(ctx);
            String in;
            in = new OkHttp(ctx).makeGetRequest("/admin/serviceorder/service/list/" + t.getProfession_professional_id(),false);
            Z=new JSONObject(in);
            System.out.println("ORDERSSS");
                    System.out.println(in);
                    if(!Z.getString("message").equals("-1")){
                        sDA.DeleteAll();
                        if(Z.getBoolean("status")){
                            Z = Z.getJSONObject("serviceorders");
                            String states [] = {"pending","accepted","confirmed","completed","canceled","deneid"};
                            Gson gson = new Gson();
                    for (int st = 0;st<states.length;st++){
                        ArrayList<ServiceOrderE> SList = new ArrayList<>();
                        JSONObject jsonZ = Z.getJSONObject(states[st]);
                        JSONArray jA = new JSONArray(jsonZ.getString("items"));
                        for (int i=0;i<jA.length();i++){
                            ServiceOrderE se = gson.fromJson(jA.getString(i), ServiceOrderE.class);
                            SList.add(se);
                        }
                        sDA.Insert(st,SList);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "*-*";
    }
    @Override
    protected void onPostExecute(String result){}
}