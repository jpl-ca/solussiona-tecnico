package com.jmt.solussiona.task;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;

import com.jmt.solussiona.http.OkHttp;
import com.jmt.solussiona.model.data_access.TecnicoDA;
import com.jmt.solussiona.model.store.Jmstore;
import com.jmt.solussiona.view.LoginActivity;
import org.apache.http.NameValuePair;

import java.util.ArrayList;
import java.util.List;

public class CerrarSesionTask extends AsyncTask<String, String, String>{
    Context ctx;
    String LOG="InicioTask";
    public CerrarSesionTask(Context ctx){
        this.ctx=ctx;
    }
    @Override
    protected void onPreExecute(){
        super.onPreExecute();
    }
    @Override
    protected String doInBackground(String... params) {
        String Z = "-1";
        String in;
        List<NameValuePair> extra=new ArrayList<NameValuePair>();
        try{
            System.out.println("Cerrando sesion");
            in= new OkHttp(ctx).makeGetRequest("/admin/user/service/logout",false);
            System.out.println(in);
            Z = "1";
        } catch (Exception e) {
            Z = "0";
        }
        return Z;
    }

    @Override
    protected void onPostExecute(String result){
        new TecnicoDA(ctx).DeleteAll();
        new Jmstore(ctx).remove();
        Intent it = new Intent(ctx,LoginActivity.class);
        it.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |Intent.FLAG_ACTIVITY_CLEAR_TASK |Intent.FLAG_ACTIVITY_NEW_TASK);
        ctx.startActivity(it);
    }
}