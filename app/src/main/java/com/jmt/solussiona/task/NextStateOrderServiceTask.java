package com.jmt.solussiona.task;

import android.content.Context;
import android.os.AsyncTask;
import com.jmt.solussiona.http.OkHttp;
import com.jmt.solussiona.model.data_access.TecnicoDA;
import com.jmt.solussiona.model.entity.TecnicoE;
import com.jmt.solussiona.util.S;
import org.json.JSONObject;

public class NextStateOrderServiceTask extends AsyncTask<String, String, String>{
    Context ctx;
    public NextStateOrderServiceTask(Context ctx){
        this.ctx = ctx;
    }
    @Override
    protected void onPreExecute(){
        super.onPreExecute();
    }
    @Override
    protected String doInBackground(String... params) {
        TecnicoE t = new TecnicoDA(ctx).Select().get(0);
        JSONObject Z = new JSONObject();
        String in;
        try{
            JSONObject value = new JSONObject(params[0]);
            JSONObject jo = new JSONObject();
            jo.put("serviceorder[id]", value.getString(S.ORDEN.so_id));
            jo.put("serviceorder[state_now]", value.getString(S.ORDEN.so_state));
            jo.put("serviceorder[state_new]", value.getString(S.ORDEN.state_new));
            jo.put("serviceorder[request_entity_id]", t.getId());
            jo.put("serviceorder[request_entity_type]", t.getEntity_type());
            System.out.println("**************");
            System.out.println(jo.toString());
            in = new OkHttp(ctx).makePostRequest("/admin/serviceorder/service/stateupdate",jo);
            System.out.println("<<<->>>>");
            System.out.println(in);
            //TODO Enviar al servicio para cambiar estado
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "*-*";
    }
    @Override
    protected void onPostExecute(String result){}
}