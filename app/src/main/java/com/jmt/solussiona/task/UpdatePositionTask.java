package com.jmt.solussiona.task;

import android.content.Context;
import android.os.AsyncTask;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.jmt.solussiona.http.OkHttp;
import com.jmt.solussiona.interfaz.AsyncResponseTask;
import com.jmt.solussiona.model.data_access.TecnicoDA;
import com.jmt.solussiona.model.entity.TecnicoE;
import com.jmt.solussiona.model.store.Jmstore;
import com.jmt.solussiona.util.S;

import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

public class UpdatePositionTask extends AsyncTask<String, String, JSONObject>{
    Context ctx;
    GoogleCloudMessaging gcm;
    String regId;
    String LOG="UpdPosTask";
    public UpdatePositionTask(Context ctx){
        this.ctx=ctx;
        regId="";
        gcm = GoogleCloudMessaging.getInstance(this.ctx);
    }
    @Override
    protected void onPreExecute(){
        super.onPreExecute();
    }
    @Override
    protected JSONObject doInBackground(String... params) {
        JSONObject Z = new JSONObject();
        String in;
        try{
            ArrayList<TecnicoE> TE = new TecnicoDA(ctx).Select();
            if(TE.size()==0)return Z;
            TecnicoE t = TE.get(0);
            JSONObject jo = new JSONObject();
            jo.put("professional[id]", t.getId());
            jo.put("professional[latitude]", params[0]);
            jo.put("professional[longitude]", params[1]);
            System.out.println("actualizando posicion de tecnico");
            System.out.println(jo.toString());
            in = new OkHttp(ctx).makePostRequest("/admin/professional/service/positionupdate",jo);
            System.out.println("RESULTADO");
            System.out.println(in);
            Z=new JSONObject(in);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Z;
    }
    @Override
    protected void onPostExecute(JSONObject result){}
}