package com.jmt.solussiona.task;

import android.content.Context;
import android.os.AsyncTask;
import com.jmt.solussiona.http.OkHttp;
import com.jmt.solussiona.interfaz.AsyncResponseTask;
import com.jmt.solussiona.util.S;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class LoginTask extends AsyncTask<String, String, JSONObject>{
    Context ctx;
    AsyncResponseTask delegate;
    public LoginTask(Context ctx, AsyncResponseTask delegate){
        this.ctx = ctx;
        this.delegate = delegate;
    }
    @Override
    protected void onPreExecute(){
        super.onPreExecute();
    }
    @Override
    protected JSONObject doInBackground(String... params) {
        JSONObject Z = new JSONObject();
        String in;
        try{
            JSONObject values = new JSONObject(params[0]);
            JSONObject jo = new JSONObject();
            jo.put("professional[access_type]", values.getString(S.TECNICO.access_type));
            jo.put("professional[email]", values.getString(S.TECNICO.email));
            jo.put("professional[password]", values.getString(S.TECNICO.password));
            in = new OkHttp(ctx).makePostRequest("/admin/professional/service/login",jo);
            System.out.println(in);
            Z=new JSONObject(in);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Z;
    }

    @Override
    protected void onPostExecute(JSONObject result){
        delegate.processFinish(result);
    }
}