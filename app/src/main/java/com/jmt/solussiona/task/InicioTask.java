package com.jmt.solussiona.task;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.jmt.solussiona.http.OkHttp;
import com.jmt.solussiona.interfaz.AsyncResponseTask;
import com.jmt.solussiona.model.store.Jmstore;
import com.jmt.solussiona.util.S;
import org.json.JSONObject;
import java.io.IOException;

public class InicioTask extends AsyncTask<String, String, JSONObject>{
    Context ctx;
    AsyncResponseTask delegate;
    GoogleCloudMessaging gcm;
    String regId;
    String LOG="InicioTask";
    public InicioTask(Context ctx, AsyncResponseTask delegate){
        this.ctx=ctx;
        this.delegate = delegate;
        regId="";
        gcm = GoogleCloudMessaging.getInstance(this.ctx);
    }
    @Override
    protected void onPreExecute(){
        super.onPreExecute();
    }
    @Override
    protected JSONObject doInBackground(String... params) {
        regId = new Jmstore(ctx).get(S.GCMID);
//        if(regId.equals("0")||regId.equals("")){
            try {
                if (gcm == null) {
                    gcm = GoogleCloudMessaging.getInstance(ctx);
                }
                regId = gcm.register(S.GOOGLE.PROJECT_ID);
                new Jmstore(ctx).push(S.GCMID,regId);
            } catch (IOException ex) {
                System.out.println("No tiene GCM ID");
                System.out.println("No tiene GCM ID");
                ex.printStackTrace();
            }
//        }
        System.out.println("*********GCM***********");
        System.out.println(regId);
        System.out.println("*********GCM***********");
        String in;
        JSONObject Z = new JSONObject();
        try{
            in = new OkHttp(ctx).makeGetRequest("/session/service/validate",false);
            Z=new JSONObject(in);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Z;
    }

    @Override
    protected void onPostExecute(JSONObject result){
        delegate.processFinish(result);
    }
}