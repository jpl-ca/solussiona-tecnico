package com.jmt.solussiona.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.gson.Gson;
import com.jmt.solussiona.R;
import com.jmt.solussiona.gcm.GPSTracker;
import com.jmt.solussiona.gcm.GcmIntentService;
import com.jmt.solussiona.interfaz.FinishListener;
import com.jmt.solussiona.model.entity.NotificacionE;
import com.jmt.solussiona.task.CerrarSesionTask;
import com.jmt.solussiona.util.S;
import com.jmt.solussiona.view.ProfessionalProfileActivity;

import java.util.Locale;

public class PanelSolicitudesFragment extends ActionBarActivity {
    Toolbar toolbar;
    ViewPager mViewPager;
    SlidingTabLayout tabs;
    SectionsPagerAdapter mSectionsPagerAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_panel_solicitud);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getResources().getString(R.string.s_solicitudes));
        getSupportActionBar().setIcon(R.drawable.ic_white);
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(mSectionsPagerAdapter);
        tabs = (SlidingTabLayout) findViewById(R.id.tabs);
        tabs.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(R.color.tabsScrollColor);
            }
            @Override
            public int getDividerColor(int position) {
                return 0;
            }
        });
        tabs.setViewPager(mViewPager);
    }
    @Override
    protected void onStart() {
        super.onStart();
        onReady();
        TrackerProfessional();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_user) {
            Intent it = new Intent(getBaseContext(), ProfessionalProfileActivity.class);
            startActivity(it);
            return true;
        }
        if (id == R.id.action_logout) {
            CerrarSesionTask cs = new CerrarSesionTask(this);
            cs.execute();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    public void TrackerProfessional(){
        GPSTracker gps = new GPSTracker(this);
//        startService(new Intent(this,GPSTracker.class));
    }
    public class SectionsPagerAdapter extends FragmentPagerAdapter {
        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }
        @Override
        public Fragment getItem(int position) {
            Fragment fr = null;
            switch (position){
                case 0:fr = new PendientesFragment();break;
                case 1:fr = new AceptadoFragment();break;
                case 2:fr = new ConfirmadoFragment();break;
                case 3:fr = new FinalizadoFragment();break;
            }
            return fr;
        }
        @Override
        public int getCount() {
            return 4;
        }
        @Override
        public CharSequence getPageTitle(int position) {
            Locale l = Locale.getDefault();
            switch (position) {
                case 0:
                    return getString(R.string.s_pendientes).toUpperCase(l);
                case 1:
                    return getString(R.string.s_aceptados).toUpperCase(l);
                case 2:
                return getString(R.string.s_confirmados).toUpperCase(l);
                case 3:
                    return getString(R.string.s_finalizados).toUpperCase(l);
            }
            return null;
        }
    }
    private void onReady() {
        final GcmIntentService gcm = new GcmIntentService();
        gcm.setContext(this);
        Bundle extras = getIntent().getExtras();
        if(extras==null||extras.isEmpty())return;
        final String extra_gcm = extras.getString(S.extra_gcm);
        final NotificacionE n = new Gson().fromJson(extra_gcm,NotificacionE.class);
        final int estado = n.getState();
        final int notification = n.getNotification_id();
        final String msj = extras.getString(n.getMsj());
        if(estado==S.ESTADO.PENDIENTE){
            mViewPager.setCurrentItem(0);
        }else if(estado==S.ESTADO.CONFIRMADO){
            mViewPager.setCurrentItem(2);
        }else if(estado==S.ESTADO.FINALIZADO){
            mViewPager.setCurrentItem(3);
        }
    }
}