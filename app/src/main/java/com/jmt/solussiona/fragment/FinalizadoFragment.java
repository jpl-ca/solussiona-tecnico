package com.jmt.solussiona.fragment;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.hudomju.swipe.SwipeToDismissTouchListener;
import com.hudomju.swipe.adapter.ListViewAdapter;
import com.hudomju.swipe.adapter.ViewAdapter;
import com.jmt.solussiona.R;
import com.jmt.solussiona.adapter.AceptadoAdapter;
import com.jmt.solussiona.adapter.FinalizadoAdapter;
import com.jmt.solussiona.model.data_access.OrdenServicioDA;
import com.jmt.solussiona.model.entity.ServiceOrderE;
import com.jmt.solussiona.task.LoadOrderServiceTask;
import com.jmt.solussiona.util.S;

import java.util.ArrayList;

public class FinalizadoFragment extends Fragment {
    ListView lView;
    FinalizadoAdapter cAdapter;
    ArrayList<ServiceOrderE> Solicitud;
    MyReceiver mReceiver;
    private SwipeRefreshLayout mSwipeRefreshLV = null;
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_solicitudes_completed, container, false);
        lView = (ListView)v.findViewById(R.id.ordersList);
        fillListView();
        mSwipeRefreshLV= (SwipeRefreshLayout) v.findViewById(R.id.swipeRefreshLV);
        mSwipeRefreshLV.setColorScheme(android.R.color.holo_green_dark,android.R.color.holo_orange_dark,android.R.color.holo_red_dark,android.R.color.holo_blue_dark);
        mSwipeRefreshLV.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshContent();
            }
        });
        return v;
    }
    private void fillListView(){
        Solicitud = new OrdenServicioDA(getActivity()).getCompleted();
        cAdapter = new FinalizadoAdapter(getActivity(),Solicitud);
        lView.setAdapter(cAdapter);
        lView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView parent, View view, int position, long id) {
                    AlertDialog alertDialog;
                    AlertDialog.Builder builder;
                    LayoutInflater inflater = (LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View cv = inflater.inflate(R.layout.cardview_detalle_solicitud,null);
                    TextView tvTitulo = (TextView)cv.findViewById(R.id.title);
                    tvTitulo.setText(getString(R.string.s_finalizados));
                    final ServiceOrderE sE = Solicitud.get(position);
                    TextView tvUsuario = (TextView)cv.findViewById(R.id.usuario);
                    tvUsuario.setText(sE.getUser_firstname()+" "+sE.getUser_lastname());
                    TextView tvDireccion = (TextView)cv.findViewById(R.id.direccion);
                    tvDireccion.setText(sE.getUser_address());
                    Button btn_cancelar = (Button)cv.findViewById(R.id.btn_cancelar);
                    Button btn_llamar = (Button)cv.findViewById(R.id.btn_llamar);
                    btn_llamar.setVisibility(View.GONE);
                    btn_cancelar.setVisibility(View.GONE);
                    builder = new AlertDialog.Builder(getActivity());
                    builder.setView(cv);
                    alertDialog = builder.create();
                    alertDialog.show();
                    alertDialog.setCancelable(true);
                }
//            }
        });
    }
    private void refreshContent(){
        final LoadOrderServiceTask lrTask = new LoadOrderServiceTask(getActivity());
        lrTask.execute();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                    String g = lrTask.get();
                    if(g.length()>=0)
                        fillListView();
                    if (mSwipeRefreshLV.isRefreshing()) {
                        mSwipeRefreshLV.setRefreshing(false);
                    }
                }catch (Exception e) {}
            }},500);
    }
    @Override
    public void onResume() {
        super.onResume();
        mReceiver = new MyReceiver();
        IntentFilter filter = new IntentFilter(S.BROADCAST.order_list);
        getActivity().registerReceiver(mReceiver, filter);
    }
    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(mReceiver);
    }
    public class MyReceiver extends BroadcastReceiver {
        public MyReceiver() {
        }
        @Override
        public void onReceive(Context context, Intent intent) {
            refreshContent();
        }
    }
    private void sendBroadcast(){
        Intent inboxIntent = new Intent(S.BROADCAST.order_list);
        getActivity().sendBroadcast(inboxIntent);
    }
}