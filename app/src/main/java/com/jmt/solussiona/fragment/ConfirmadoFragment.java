package com.jmt.solussiona.fragment;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.jmt.solussiona.R;
import com.jmt.solussiona.adapter.AceptadoAdapter;
import com.jmt.solussiona.adapter.ConfirmadoAdapter;
import com.jmt.solussiona.dialog.CancelRequestDialog;
import com.jmt.solussiona.interfaz.FinishListener;
import com.jmt.solussiona.model.data_access.OrdenServicioDA;
import com.jmt.solussiona.model.entity.ServiceOrderE;
import com.jmt.solussiona.task.LoadOrderServiceTask;
import com.jmt.solussiona.util.S;

import java.util.ArrayList;

public class ConfirmadoFragment extends Fragment {
    ListView lView;
    ConfirmadoAdapter cAdapter;
    ArrayList<ServiceOrderE> Solicitud;
    private SwipeRefreshLayout mSwipeRefreshLV = null;
    AlertDialog alertDialog;
    MyReceiver mReceiver;
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_solicitudes, container, false);
        lView = (ListView)v.findViewById(R.id.ordersList);
        lView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                AlertDialog.Builder builder;
                LayoutInflater inflater = (LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View cv = inflater.inflate(R.layout.cardview_detalle_solicitud,null);
                TextView tvTitulo = (TextView)cv.findViewById(R.id.title);
                tvTitulo.setText(getString(R.string.s_confirmados));
                final ServiceOrderE sE = Solicitud.get(position);
                TextView tvUsuario = (TextView)cv.findViewById(R.id.usuario);
                tvUsuario.setText(sE.getUser_firstname()+" "+sE.getUser_lastname());
                TextView tvDireccion = (TextView)cv.findViewById(R.id.direccion);
                tvDireccion.setText(sE.getUser_address());
                Button btn_cancelar = (Button)cv.findViewById(R.id.btn_cancelar);
                btn_cancelar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        new CancelRequestDialog(getActivity()).show(sE,new FinishListener() {
                            @Override
                            public void processFinish() {
                                alertDialog.dismiss();
                                refreshContent();
                            }
                        });
                    }
                });
                Button btn_llamar = (Button)cv.findViewById(R.id.btn_llamar);
                btn_llamar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String PhoneNo = sE.getUser_phone();
                        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + PhoneNo));
                        startActivity(intent);
                    }
                });
                builder = new AlertDialog.Builder(getActivity());
                builder.setView(cv);
                alertDialog = builder.create();
                alertDialog.show();
                alertDialog.setCancelable(true);
            }
        });
        fillListView();
        mSwipeRefreshLV= (SwipeRefreshLayout) v.findViewById(R.id.swipeRefreshLV);
        mSwipeRefreshLV.setColorScheme(android.R.color.holo_green_dark,android.R.color.holo_orange_dark,android.R.color.holo_red_dark,android.R.color.holo_blue_dark);
        mSwipeRefreshLV.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshContent();
            }
        });
        return v;
    }
    private void refreshContent(){
        System.out.println("Refrescandooooo111");
        final LoadOrderServiceTask lrTask = new LoadOrderServiceTask(getActivity());
        lrTask.execute();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                System.out.println("Refrescandooooo222");
                try {
                    String g = lrTask.get();
                    if(g.length()>=0)
                        fillListView();
                    if (mSwipeRefreshLV.isRefreshing()) {
                        mSwipeRefreshLV.setRefreshing(false);
                    }
                }catch (Exception e) {}
            }},500);
    }
    private void fillListView(){
        Solicitud = new OrdenServicioDA(getActivity()).getConfirmed();
        cAdapter = new ConfirmadoAdapter(getActivity(),Solicitud);
        lView.setAdapter(cAdapter);
        cAdapter.notifyDataSetChanged();
    }


    @Override
    public void onResume() {
        super.onResume();
        mReceiver = new MyReceiver();
        IntentFilter filter = new IntentFilter(S.BROADCAST.order_list);
        getActivity().registerReceiver(mReceiver, filter);
    }
    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(mReceiver);
    }
    public class MyReceiver extends BroadcastReceiver {
        public MyReceiver() {
        }
        @Override
        public void onReceive(Context context, Intent intent) {
            System.out.println("Pidiendo q lo actualicen...confirmado");
            refreshContent();
        }
    }
    private void sendBroadcast(){
        Intent inboxIntent = new Intent(S.BROADCAST.order_list);
        getActivity().sendBroadcast(inboxIntent);
    }
}