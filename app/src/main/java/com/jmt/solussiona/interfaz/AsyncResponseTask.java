package com.jmt.solussiona.interfaz;

import org.json.JSONObject;

import java.util.ArrayList;

public interface AsyncResponseTask {
    void processFinish(ArrayList<?> output);
    void processFinish(JSONObject output);
}