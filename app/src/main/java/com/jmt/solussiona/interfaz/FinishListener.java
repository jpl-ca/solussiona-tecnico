package com.jmt.solussiona.interfaz;

import org.json.JSONObject;

import java.util.ArrayList;

public interface FinishListener {
    void processFinish();
}