package com.jmt.solussiona.view;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.google.gson.Gson;
import com.jmt.solussiona.R;
import com.jmt.solussiona.fragment.PanelSolicitudesFragment;
import com.jmt.solussiona.interfaz.AsyncResponseTask;
import com.jmt.solussiona.model.entity.SessionE;
import com.jmt.solussiona.task.InicioTask;
import com.jmt.solussiona.task.LoadOrderServiceTask;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;


public class SplashActivity extends ActionBarActivity {
    InicioTask iTask;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
    }

    @Override
    protected void onStart() {
        super.onStart();
        final LoadOrderServiceTask lTask = new LoadOrderServiceTask(this);
        lTask.execute();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                    if(lTask.get().length()>=0){
                        Intent it = new Intent(getBaseContext(), PanelSolicitudesFragment.class);
                        it.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(it);
                        finish();
                        overridePendingTransition(R.anim.right_in, R.anim.left_out);
                    }
                } catch (Exception e) {}
            }
        }, 1200);
    }
}