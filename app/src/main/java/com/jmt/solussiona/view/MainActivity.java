package com.jmt.solussiona.view;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.jmt.solussiona.R;
import com.jmt.solussiona.interfaz.AsyncResponseTask;
import com.jmt.solussiona.model.data_access.TecnicoDA;
import com.jmt.solussiona.model.entity.SessionE;
import com.jmt.solussiona.task.InicioTask;

import org.json.JSONObject;

import java.util.ArrayList;


public class MainActivity extends ActionBarActivity {
    InicioTask iTask;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
    }

    @Override
    protected void onStart() {
        super.onStart();
        iTask = new InicioTask(this,new AsyncResponseTask() {
            @Override
            public void processFinish(ArrayList<?> output) {}
            @Override
            public void processFinish(final JSONObject output) {
                final SessionE se =new Gson().fromJson(output.toString(), SessionE.class);
                int TIME_DELAY = 500;
                if(se.getStatus())TIME_DELAY = 1000;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        int tecSZ= new TecnicoDA(MainActivity.this).Select().size();
                        Intent i;
                        if(se.getStatus()&&tecSZ>0)
                            i = new Intent(MainActivity.this, SplashActivity.class);
                        else if(se.getMessage().equals("-1") && tecSZ>0){
                            i = new Intent(MainActivity.this, SplashActivity.class);
                            Toast.makeText(getApplicationContext(),getString(R.string.s_sin_conexion),Toast.LENGTH_LONG).show();
                        }else
                            i = new Intent(MainActivity.this, LoginActivity.class);
                        startActivity(i);
                        finish();
                    }
                }, TIME_DELAY);
            }
        });
        iTask.execute();
//        int id= android.os.Process.myPid();
//        Toast.makeText(this,"-->"+id,Toast.LENGTH_LONG).show();
    }
}