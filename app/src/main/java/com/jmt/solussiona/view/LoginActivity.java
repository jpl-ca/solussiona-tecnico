package com.jmt.solussiona.view;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.jmt.solussiona.R;
import com.jmt.solussiona.fragment.PanelSolicitudesFragment;
import com.jmt.solussiona.interfaz.AsyncResponseTask;
import com.jmt.solussiona.model.data_access.TecnicoDA;
import com.jmt.solussiona.model.entity.SessionE;
import com.jmt.solussiona.model.entity.TecnicoE;
import com.jmt.solussiona.model.store.Jmstore;
import com.jmt.solussiona.task.InicioTask;
import com.jmt.solussiona.task.LoginTask;
import com.jmt.solussiona.util.S;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.SQLOutput;
import java.util.ArrayList;


public class LoginActivity extends ActionBarActivity implements View.OnClickListener{
    Button btnIngresar;
    TextView login_email,login_password;
    TextView tv_forgot_pass;
    LoginTask isTask;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        btnIngresar = (Button)findViewById(R.id.btn_ingresar);
        btnIngresar.setOnClickListener(this);
        login_email = (TextView)findViewById(R.id.txt_correo);
        login_password = (TextView)findViewById(R.id.txt_pass);
        tv_forgot_pass = (TextView)findViewById(R.id.tv_forgot_pass);
        tv_forgot_pass.setOnClickListener(this);
    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tv_forgot_pass:forgot_password();break;
            case R.id.btn_ingresar:iniciarsesion();break;
        }
    }

    private void forgot_password() {

    }

    private void iniciarsesion(){
        String email = login_email.getText().toString();
        String password = login_password.getText().toString();
        btnIngresar.setEnabled(false);
        if (esValido(email,password)){
            isTask = new LoginTask(this,new AsyncResponseTask() {
                @Override
                public void processFinish(ArrayList<?> output) {}
                @Override
                public void processFinish(JSONObject output) {
                    btnIngresar.setEnabled(true);
                    try {
                        boolean r = output.getBoolean("status");
                        if(r){
                            new Jmstore(LoginActivity.this).push(S.TOKEN,output.getString(S.TOKEN));
                            JSONObject professionalJ = new JSONObject(output.getString("professional"));
                            JSONArray professionsJA = new JSONArray(professionalJ.getString("professions"));
                            if(professionsJA.length()==0){
                                Toast.makeText(LoginActivity.this, getString(R.string.s_acceso_no_permitido), Toast.LENGTH_SHORT).show();
                                finish();
                            }else{
                                int id = professionsJA.getJSONObject(0).getInt("pp_id");
                                String profesison_name = professionsJA.getJSONObject(0).getString("name");
                                System.out.println(id);
                                System.out.println(profesison_name);
                                TecnicoE tec = new Gson().fromJson(output.getString("professional"),TecnicoE.class);
                                tec.setProfession_professional_id(id);
                                tec.setProfession_professional_name(profesison_name);
                                new TecnicoDA(LoginActivity.this).Insert(tec);
                                Intent it = new Intent(getBaseContext(),SplashActivity.class);
                                it.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |Intent.FLAG_ACTIVITY_CLEAR_TASK |Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(it);
                                finish();
                                overridePendingTransition(R.anim.right_in, R.anim.left_out);
                            }
                        }else{
                            String msg = output.getString("message");
                            if(msg.equals("-1"))msg = getString(R.string.s_error_coneccion);
                            Toast.makeText(LoginActivity.this,msg,Toast.LENGTH_LONG).show();
                        }
                    }catch (JSONException e){
                        e.printStackTrace();
                    };
                }
            });
            TecnicoE t = new TecnicoE();
            t.setAccess_type(S.TIPO_ACCESO.EMAIL);
            t.setEmail(email);
            t.setPassword(password);
            isTask.execute(new Gson().toJson(t).toString());
        }
    }
    private boolean esValido(String email,String pass){
        if (TextUtils.isEmpty(email)||TextUtils.isEmpty(pass)||pass.length()<3){
            Toast.makeText(this,getResources().getString(R.string.s_datos_incompletos),Toast.LENGTH_LONG).show();
            return false;
        }
        if(!android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            Toast.makeText(this, getResources().getString(R.string.s_ingrese_correo_electronico_valido), Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }
}