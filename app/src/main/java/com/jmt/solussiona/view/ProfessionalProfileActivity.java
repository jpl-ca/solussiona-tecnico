package com.jmt.solussiona.view;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.jmt.solussiona.R;
import com.jmt.solussiona.http.OkHttp;
import com.jmt.solussiona.model.data_access.TecnicoDA;
import com.jmt.solussiona.model.entity.TecnicoE;
import com.jmt.solussiona.util.S;
import com.squareup.picasso.Picasso;

public class ProfessionalProfileActivity extends ActionBarActivity implements View.OnClickListener{
    Button btnSalir;
    TextView login_email,nombre,apellidos,celular,profesion;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_professional_profile);
        initComponents();
    }

    private void initComponents() {
        TecnicoE tecnico = new TecnicoDA(this).Select().get(0);
        nombre= (TextView)findViewById(R.id.nombre);
        apellidos = (TextView)findViewById(R.id.apellidos);
        profesion = (TextView)findViewById(R.id.profesion);
        login_email = (TextView)findViewById(R.id.correo);
        celular = (TextView)findViewById(R.id.telefono);
        ImageView img_profile = (ImageView)findViewById(R.id.image_profile);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getResources().getString(R.string.s_mi_perfil));
        getSupportActionBar().setIcon(R.drawable.ic_white);

        nombre.setText(tecnico.getFirstname());
        apellidos.setText(tecnico.getLastname());
        login_email.setText(tecnico.getEmail());
        celular.setText(tecnico.getMobile());
        profesion.setText(tecnico.getProfession_professional_name());

        String url = OkHttp.URL+"/images/professionals/"+ tecnico.getId()+"."+tecnico.getImageProfile();
        if(url!=null&&url.length()>0)
            Picasso.with(getBaseContext()).load(url).placeholder(R.drawable.user_profile).into(img_profile);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
        }
    }
    private void salir(){
        finish();
    }

    @Override
    protected void onPause() {
        super.onPause();
        salir();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        salir();
    }
}