package com.jmt.solussiona.gcm;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.RemoteViews;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.gson.Gson;
import com.jmt.solussiona.R;
import com.jmt.solussiona.fragment.PanelSolicitudesFragment;
import com.jmt.solussiona.model.entity.NotificacionE;
import com.jmt.solussiona.model.store.Jmstore;
import com.jmt.solussiona.task.LoadOrderServiceTask;
import com.jmt.solussiona.util.S;

public class GcmIntentService extends IntentService {
    public int NOTIFICATION_ID = 1;
    public boolean IS_ONGOING = false;
    private NotificationManager mNotificationManager;
    Context ctx;
    NotificationCompat.Builder builder;

    public GcmIntentService() {
        super("GcmIntentService");
        ctx = this;
    }

    public static final String TAG = "GCMNotificationIntentService-Solussssssiona";

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.d(TAG, "onHandleIntent " + intent.getDataString());
        Bundle extras = intent.getExtras();
        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(ctx);
        String messageType = gcm.getMessageType(intent);
        if (extras != null) {
            if (!extras.isEmpty()) {
                if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR.equals(messageType)) {
                    System.out.println("Send error: " + extras.toString());
                } else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED.equals(messageType)) {
                    System.out.println("Deleted messages on server: " + extras.toString());
                } else if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType)) {
                    System.out.println("***********W************");
                    System.out.println(extras.toString());
                    System.out.println("***********W************");
                    String title = extras.getString("title");
                    String msj = extras.getString("message");
                    int state = Integer.parseInt(extras.getString("state"));
                    int state_old = Integer.parseInt(extras.getString("state_old"));
                    int so_id = -1;
                    if(extras.containsKey("so_id"))
                        so_id = Integer.parseInt(extras.getString("so_id"));
                    int sr_id = Integer.parseInt(extras.getString("sr_id"));

                    if((state_old!=S.ESTADO.PENDIENTE&&state==S.ESTADO.CANCELADO)||state!=S.ESTADO.CANCELADO){
                        NotificacionE nE = new NotificacionE(title,msj,state,so_id,sr_id);
                        sendNotification(nE);
                    }
                    new LoadOrderServiceTask(this).execute();
                    ///// Send BroadCast
                    Intent inboxIntent = new Intent(S.BROADCAST.order_list);
                    sendBroadcast(inboxIntent);
                }
            }
        }
        GcmBroadcastReceiver.completeWakefulIntent(intent);
    }

    public void sendNotification(NotificacionE n) {
        NOTIFICATION_ID = new Jmstore(ctx).nextInt();
        n.setNotification_id(NOTIFICATION_ID);
        mNotificationManager = (NotificationManager) ctx.getSystemService(NOTIFICATION_SERVICE);
        Intent intent = new Intent(ctx, PanelSolicitudesFragment.class);
        NotificationCompat.Builder notifB =
                new NotificationCompat.Builder(ctx)
                        .setContentTitle(n.getTitle())
                        .setContentText(n.getMsj())
                        .setSmallIcon(R.drawable.ic_white);
        notifB.setDefaults(Notification.DEFAULT_SOUND);
        intent.putExtra(S.extra_gcm, new Gson().toJson(n));
        PendingIntent pIntent = PendingIntent.getActivity(ctx, NOTIFICATION_ID, intent, 0);
        Notification notification = notifB.setContentIntent(pIntent).build();
        notification.defaults |= Notification.DEFAULT_VIBRATE;
        notification.flags = Notification.DEFAULT_LIGHTS | Notification.FLAG_AUTO_CANCEL;
        RemoteViews contentView = new RemoteViews(this.getPackageName(), R.layout.notification_inbox);
        contentView.setTextViewText(R.id.tv_title,n.getTitle());
        contentView.setTextViewText(R.id.tv_msj,n.getMsj());
        notification.contentView = contentView;

        mNotificationManager.notify(NOTIFICATION_ID, notification);
    }

    public void cancel(int MY_NOTIFICATION_ID) {
        mNotificationManager = (NotificationManager) ctx.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.cancel(MY_NOTIFICATION_ID);
    }

    public void setContext(Context _ctx) {
        ctx = _ctx;
    }
}