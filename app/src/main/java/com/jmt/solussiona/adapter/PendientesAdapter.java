package com.jmt.solussiona.adapter;

/**
 * Created by JMTech-Android on 01/04/2015.
 */

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.jmt.solussiona.R;
import com.jmt.solussiona.model.entity.ServiceOrderE;

import java.util.ArrayList;

public class PendientesAdapter extends BaseAdapter {
    private Activity activity;
    private ArrayList<ServiceOrderE> data;
    private static LayoutInflater inflater=null;

    public PendientesAdapter(Activity a, ArrayList<ServiceOrderE> d) {
        activity=a;
        data=d;
        inflater=(LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    public int getCount() {
        return data.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        View vi=convertView;
        if(convertView==null) vi = inflater.inflate(R.layout.listview_orders, null);
        TextView tvUser = (TextView)vi.findViewById(R.id.txt_user);
        TextView tvAddress = (TextView)vi.findViewById(R.id.txt_direcc);
        TextView tvFecha = (TextView)vi.findViewById(R.id.tvFecha);
        ServiceOrderE servO = data.get(position);
        tvUser.setText(servO.getUser_firstname()+" "+servO.getUser_lastname());
        tvAddress.setText(servO.getUser_address());
        String fecha = data.get(position).getSo_created().getDate();
        tvFecha.setText(fecha.substring(0,fecha.length()-3));
        return vi;
    }
}