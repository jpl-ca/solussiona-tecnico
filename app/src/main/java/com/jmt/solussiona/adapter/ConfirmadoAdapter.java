package com.jmt.solussiona.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.jmt.solussiona.R;
import com.jmt.solussiona.model.entity.ServiceOrderE;

import java.util.ArrayList;

/**
 * Created by JMTech-Android on 20/03/2015.
 */
public class ConfirmadoAdapter extends BaseAdapter {
    private Activity activity;
    private ArrayList<ServiceOrderE> data;
    private static LayoutInflater inflater=null;

    public ConfirmadoAdapter(Activity a, ArrayList<ServiceOrderE> d) {
        activity=a;
        data=d;
        inflater=(LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    public int getCount() {
        return data.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        View vi=convertView;
        if(convertView==null) vi = inflater.inflate(R.layout.listview_orders, null);
        ServiceOrderE s = data.get(position);
        TextView tvUser = (TextView)vi.findViewById(R.id.txt_user);
        TextView tvAddress = (TextView)vi.findViewById(R.id.txt_direcc);
        TextView tvFecha = (TextView)vi.findViewById(R.id.tvFecha);
        tvUser.setText(s.getUser_firstname()+" "+s.getUser_lastname());
        tvAddress.setText(s.getUser_address());
        String fecha = data.get(position).getSo_created().getDate();
        tvFecha.setText(fecha.substring(0,fecha.length()-3));
        return vi;
    }
}