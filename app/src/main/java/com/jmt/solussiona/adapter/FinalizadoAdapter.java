package com.jmt.solussiona.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.jmt.solussiona.R;
import com.jmt.solussiona.model.entity.ServiceOrderE;

import java.util.ArrayList;

/**
 * Created by JMTech-Android on 20/03/2015.
 */
public class FinalizadoAdapter extends BaseAdapter {
    private Activity activity;
    private ArrayList<ServiceOrderE> data;
    private static LayoutInflater inflater=null;

    public FinalizadoAdapter(Activity a, ArrayList<ServiceOrderE> d) {
        activity=a;
        data=d;
        inflater=(LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    public int getCount() {
        return data.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }
    public void remove(int position) {
        data.remove(position);
        notifyDataSetChanged();
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = convertView == null
                ? new ViewHolder(convertView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.listview_orders, parent, false))
                : (ViewHolder) convertView.getTag();
        ServiceOrderE s = data.get(position);
        viewHolder.txt_user.setText(s.getUser_firstname()+" "+s.getUser_lastname());
        viewHolder.txt_direcc.setText(s.getUser_address());
        String fecha = data.get(position).getSo_created().getDate();
        viewHolder.tvFecha.setText(fecha.substring(0,fecha.length()-3));
        return convertView;
    }

    static class ViewHolder {
        TextView txt_user,txt_direcc,tvFecha;
        ViewHolder(View view) {
            txt_user = ((TextView) view.findViewById(R.id.txt_user));
            txt_direcc = ((TextView) view.findViewById(R.id.txt_direcc));
            tvFecha = ((TextView) view.findViewById(R.id.tvFecha));
            view.setTag(this);
        }
    }
}