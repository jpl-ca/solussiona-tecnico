package com.jmt.solussiona.util;

/**
 * Created by JMTech-Android on 31/03/2015.
 */
public final class S {
    public static final String TOKEN = "token";
    public static final String COOKIE = "Set-Cookie";
    public static final String GCMID = "Gcmid";
    public static final String extra_gcm = "extra_gcm";
    public static final class GOOGLE {
        public static final String PROJECT_ID = "381878516521";
        public static final String GCM_ID = "gcm_id";
    }
    public static final class TIPO_ACCESO {
        public static final String EMAIL = "1";
        public static final String FACEBOOK = "2";
        public static final String TWITER = "3";
        public static final String GPLUS = "4";
    }
    public static final class ORDEN{
        public static final String so_id="so_id";
        public static final String so_state="so_state";
        public static final String sr_id="sr_id";
        public static final String user_id="user_id";
        public static final String user_firstname="user_firstname";
        public static final String user_lastname="user_lastname";
        public static final String user_address="user_address";
        public static final String user_phone="user_phone";
        public static final String detail="detail";
        public static final String state_new="state_new";
        public static final String request_entity_id="request_entity_id";
        public static final String request_entity_type="request_entity_type";
    }
    public static final class TECNICO{
        public static final String id="id";
        public static final String firstname="firstname";
        public static final String lastname="lastname";
        public static final String email="email";
        public static final String phone="phone";
        public static final String mobile="mobile";
        public static final String imageProfile="imageProfile";
        public static final String latitude="latitude";
        public static final String longitude="longitude";
        public static final String state="state";
        public static final String entity_type="entity_type";
        public static final String password="password";
        public static final String access_type="access_type";
        public static final String profession_professional_id="profession_professional_id";
        public static final String profession_professional_name="profession_professional_name";
    }
    public static final class ESTADO {
        public static final int ACTIVO = 1;
        public static final int INACTIVO = 2;
        public static final int BLOQUEADO = 3;
        public static final int PENDIENTE = 4;
        public static final int ACEPTADO = 5;
        public static final int CONFIRMADO = 6;
        public static final int EJECUCION = 7;
        public static final int FINALIZADO = 8;
        public static final int CANCELADO = 9;
        public static final int RECHAZADO = 10;
    }
    public static final class BROADCAST {
        public static final String order_list="com.jmt.solussiona.fragment.order_list";
    }
}
