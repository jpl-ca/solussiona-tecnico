package com.jmt.solussiona.dialog;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.widget.Toast;

import com.google.gson.Gson;
import com.jmt.solussiona.R;
import com.jmt.solussiona.interfaz.FinishListener;
import com.jmt.solussiona.model.entity.ServiceOrderE;
import com.jmt.solussiona.task.NextStateOrderServiceTask;
import com.jmt.solussiona.util.S;

/**
 * Created by JMTech-Android on 06/04/2015.
 */
public class CancelRequestDialog extends AlertDialog.Builder {
    NextStateOrderServiceTask nTask;
    public CancelRequestDialog(Context context, int theme) {
        super(context, theme);
    }
    public CancelRequestDialog(Context context) {
        super(context);
    }
    public void show(final ServiceOrderE so,final FinishListener fl){
        nTask = new NextStateOrderServiceTask(getContext());
        setIcon(R.drawable.ic_launcher);
        setTitle(getContext().getString(R.string.s_cancelar_orden_servicio));
        setMessage(R.string.s_esta_seguro_cancelar_orden_servicio);
        setPositiveButton(getContext().getString(R.string.s_si),new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                so.setState_new(S.ESTADO.CANCELADO);
                nTask.execute(new Gson().toJson(so));
                try {
                    if(nTask.get().length()>0)
                        fl.processFinish();
                }catch (Exception e){}
            }
        });
        setNegativeButton(getContext().getString(R.string.s_no),new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {}
        });
        show();
    }
}